package com.softserve;

import freemarker.template.Configuration;
import freemarker.template.Version;
import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.FlywayException;
import spark.ModelAndView;
import spark.template.freemarker.FreeMarkerEngine;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static spark.Spark.get;
import static spark.Spark.port;

public class Application {
    public static void main(String[] args) {
        port(8080);
        migration();
        get("template-example", (req, res) -> {
            Map<String, Object> model = new HashMap<>();
            model.put("id", UUID.randomUUID());
            return render(model, "helloworld.html");
        });
    }

// declare this in a util-class

    public static String render(Map<String, Object> model, String templatePath) throws IOException {

        Configuration config = new Configuration(new Version("2.3.26"));
        URL tmpl = Application.class.getClassLoader().getResource("tmpl");
        config.setDirectoryForTemplateLoading(new File(tmpl.getPath()));
        return new FreeMarkerEngine(config).render(new ModelAndView(model, templatePath));
    }

    public static void migration() throws FlywayException {
        Flyway flyway = new Flyway();
        flyway.setDataSource("jdbc:mysql://localhost:3306/museum?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "root", "root");
        flyway.migrate();
    }
}
